import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    logged: !!localStorage.getItem('tok'),
    user: JSON.parse(localStorage.getItem('user') || null),
  },
  mutations: {
  },
  actions: {
    logout ({ state }) {
      state.logged = false;
      state.user = null;
      localStorage.clear();
    },
    login ({ state }, data) {
      localStorage.setItem('user', JSON.stringify(data.user));
      localStorage.setItem('tok', data.tok);
      state.user = data.user;
      state.logged = !!data.tok;
    },
  },
  getters: {
    loggedAndUnverified (state) {
      return state.user && !state.user.verified;
    },
  },
});
