import Vue from 'vue';
import App from './App.vue';
import router from './router';
import store from './store';
import axios from 'axios';
import './assets/style.css';
import 'vue-awesome/icons';
import Icon from 'vue-awesome/components/Icon';
import Error from '@/components/Error.vue';

Vue.config.productionTip = false;

Vue.component('v-icon', Icon);
Vue.component('Error', Error);

axios.defaults.baseURL = process.env.VUE_APP_URL;

new Vue({
  router,
  store,
  render: h => h(App),
}).$mount('#app');
