import Vue from 'vue';
import VueRouter from 'vue-router';
import Home from '@/views/Home.vue';
import Register from '@/views/Register.vue';
import Login from '@/views/Login.vue';
import LoginValidate from '@/components/LoginValidate.vue';
import store from '@/store';

Vue.use(VueRouter);

const routes = [
  {
    path: '/',
    name: 'home',
    component: Home,
    meta: {
      requiresAuth: true,
    },
  },
  {
    path: '/register',
    name: 'register',
    component: Register,
  },
  {
    path: '/login',
    name: 'login',
    component: Login,
    children: [
      {
        path: 'validate',
        component: LoginValidate,
      },
    ],
  },
  {
    path: '/register_42_callback',
    redirect: (to) => {
      const query = { 'intra_code': to.query.code };
      return { path: '/register', query };
    },
  },
  {
    path: '/logout',
    beforeEnter (_to, _from, next) {
      store.dispatch('logout');
      next('/login');
    },
  },
  // {
  //   path: '*',
  //   component: () => ({
  //     template: '<div>404</div>',
  //     beforeEnter () {
  //       console.log('Before 404');
  //     },
  //   }),
  // },
];

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes,
});

router.beforeEach((to, _from, next) => {
  if (to.matched.some((r) => r.meta.requiresAuth)) {
    if (!store.state.logged) return next('/login');
    if (store.state.user && !store.state.user.verified) {
      return next('/login/validate');
    }
  }
  return next();
});

export default router;
