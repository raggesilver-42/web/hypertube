const router  = require('express').Router();

const routes = [
  { url: '/auth', path: 'auth.js' },
];

for (const route of routes) {
  router.use(route.url, require(`./routes/${route.path}`));
}

/**
 * This is the global route error handler. Any route might call next(error) and
 * that error will be handled here
 */
// eslint-disable-next-line no-unused-vars
router.use((err, req, res, next) => {
  console.log(err);
  return res.status(500).json({ error: 'INTERNAL_ERROR' });
});

module.exports = router;
