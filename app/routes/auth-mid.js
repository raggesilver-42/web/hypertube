const User = require('../models/user');

const {
  reqparams, notEmpty, unique, validId,
} = require('@raggesilver/reqparams');

// eslint-disable-next-line no-useless-escape
const mailReg = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
const passReg = /^(?=.*[a-zA-Z])(?=.*[0-9])(?=.*[~!@#$%^&*()_+-]).{8,}$/;
const userReg = /^\w{6,20}$/;

module.exports = {
  postRegister: [
    (req, _res, next) => {
      console.log(req.body);
      next();
    },
    reqparams({
      email: {
        validate: [
          (val) => mailReg.test(val),
          async (val) => await unique(val, 'email', User),
        ],
        type: String,
      },
      username: {
        validate: [
          (val) => userReg.test(val),
          async (val) => await unique(val, 'username', User),
        ],
        type: String,
        msg: 'Username must have 6-20 characters (a-z, A-Z, 0-9, _)',
      },
      password: {
        validate: (val) => passReg.test(val),
        type: String,
        msg: 'Password must have at least 8 characters, one special character ' +
          'and one number',
      },
      'name.first': {
        validate: notEmpty,
        type: String,
      },
      'name.last': {
        validate: notEmpty,
        type: String,
      },
    }),
  ],
  postLogin: reqparams({
    username: { either: 1 },
    email:    { either: 1 },
    password: {},
  }),
  postSend: reqparams({
    email: {},
  }),
  postVerify: reqparams({
    token: {
      validate: validId,
      msg: 'Invalid token',
    },
  }),
};
