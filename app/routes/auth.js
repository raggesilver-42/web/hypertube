const router = require('express').Router();
const axios  = require('axios');
const User   = require('../models/user');
const Middle = require('./auth-mid');

// Intra login
router.get('/intra_auth/:code', async (req, res, next) => {
  try {
    /**
     * Loging in with intra works like this:
     *
     * - the client is redirected to intra's login page
     * - comes back to our client with a code (?code=...)
     * - the client makes a POST request to this route with that code (:code)
     * - this route exchanges the code for an access_token
     * - then uses that access_token to get the user's information
     *
     * As this is a login route it will only return positive if the user already
     * had an account. If they didn't, all the information that could be reused
     * for the login will be sent to the client so that the register form can be
     * automatically partially filled.
     */

    let { data } = await axios.post('https://api.intra.42.fr/oauth/token', {
      grant_type: 'authorization_code',
      client_id: process.env.INTRA_CLIENT_ID,
      client_secret: process.env.INTRA_CLIENT_SECRET,
      code: req.params.code,
      state: '2b16f8b42f3ea4811a11b3f2d10df58e',
      // This url has to be the same redirect url used in the client request
      redirect_uri: 'http://localhost:8080/register_42_callback',
    });

    let { data: _user } = await axios.get('https://api.intra.42.fr/v2/me', {
      headers: {
        'Authorization': `Bearer ${data.access_token}`,
      },
    });

    let [ user, fromUsername ] = await Promise.all([
      User.findOne({ email: _user.email }),
      User.findOne({ username: _user.email.split('@')[0] }),
    ]);

    if (user) {
      // Successful login
      return res.status(200).json({
        user: user.getPublicData(),
        tok: user.getToken(),
      });
    }
    else {
      let info = {
        user: {
          email: _user.email,
          name: { first: _user.first_name, last: _user.last_name },
        },
      };
      if (!fromUsername) {
        info.user.username = _user.email.split('@')[0];
      }
      // User not registered yet
      return res.status(442).json(info);
    }
  }
  catch (err) {
    // TODO: Handle errors here to provide decent error messages to the user
    next(err);
  }
});

router.post('/register', Middle.postRegister, async (req, res, next) => {
  let user = new User({
    email: req.body.email,
    username: req.body.username,
    name: {
      first: req.body.name.first,
      last: req.body.name.last,
    },
    password: await User.hashPassword(req.body.password),
  });

  try {
    await user.save();
    await user.sendVerificationCode();

    return res.json({
      user: user.getPersonalData(),
      tok: user.getToken(),
    });
  }
  catch (e) { return next(e); }
});

router.post('/login', Middle.postLogin, async (req, res, next) => {
  try {
    let payload = {};

    if (req.body.email) payload.email = req.body.email;
    else payload.username = req.body.username;

    let user = await User.findOne(payload);

    if (!user || !await user.comparePassword(req.body.password)) {
      return res.status(401).json({ error: 'Invalid credentials' });
    }

    return res.json({
      user: user.getPersonalData(),
      tok: user.getToken(),
    });
  }
  catch (e) { return next(e); }
});

function getRandomInt (min, max) {
  min = Math.ceil(min);
  max = Math.floor(max);
  Math.floor(Math.random() * (max - min + 1)) + min;
}

router.post('/send_verification', Middle.postSend, async (req, res, next) => {
  try {
    let user = await User.findOne({ email: req.body.email });

    if (user) {
      if (user.verified) return res.json({ verified: true });

      await user.sendVerificationCode();
      return res.json({});
    }
    else {
      // There is no user registered with that email, as another prevention of
      // bruteforce email discovery we wait 1~6s to respond
      setTimeout(() => {
        return res.json({});
      }, getRandomInt(1, 6) * 1000);
    }
  }
  catch (e) { return next(e); }
});

router.post('/verify', Middle.postVerify, async (req, res, next) => {
  try {
    let user = await User.findOne({ 'verification.code.tok': req.body.token });

    if (user && new Date(user.verification.code.exp) < new Date()) {
      user.verification.code = null;
      user.verified = true;

      await user.save();
    }
    else {
      return res.status(400).json({ error: 'Invalid token' });
    }
  }
  catch (e) { return next(e); }
});

module.exports = router;
