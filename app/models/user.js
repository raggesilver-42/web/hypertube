const mongoose  = require('mongoose');
const jwt       = require('jsonwebtoken');
const bcrypt    = require('bcrypt');
const crypto    = require('crypto');
const Mailer    = require('../modules/mailer');

const schema = new mongoose.Schema({
  email:    { type: String },
  username: { type: String },
  password: { type: String },
  name: {
    first:  { type: String },
    last:   { type: String },
  },
  settings: {
    language: { type: String, default: 'english' },
  },
  picture: { type: String, default: 'https://imgur.com/IqlHUiV.png' },
  // TODO: change this to ObjectId array ref'ed to Video model
  likes:   { type: Array },
  verified:     { type: Boolean, default: false },
  verification: {
    code: {
      tok: { type: String },
      exp: { type: Date },
    },
    reset: {
      tok: { type: String },
      exp: { type: Date },
    },
  },
});

schema.statics.hashPassword = async (password) => {
  return await bcrypt.hash(password, 10);
};

schema.methods.comparePassword = async function (password) {
  return await bcrypt.compare(password, this.password);
};

schema.methods.getToken = function () {
  return jwt.sign({
    _id: this._id,
  }, process.env.JWT_KEY);
};

schema.statics.generateVerificationCode = () => {
  let code = {
    tok: crypto.randomBytes(20).toString('hex'),
    exp: new Date(),
  };

  code.exp.setHours(code.exp.getHours() + 1);

  return (code);
};

schema.statics.generateResetCode = () => {
  let code = {
    tok: crypto.randomBytes(20).toString('hex'),
    exp: new Date(),
  };

  code.exp.setHours(code.exp.getHours() + 1);

  return (code);
};

/**
 * Generate a new reset code and send it to the user's email.
 * Throws Mailer/Mongoose errors.
 */
schema.methods.sendResetCode = async function () {
  this.verification.reset = this.constructor.generateResetCode();
  await this.save();

  return await Mailer.sendFromTemplate('passwordreset', {
    from: process.env.MAIL_USER,
    to: this.email,
    subject: 'Password reset',
  }, { tok: this.verification.reset.tok, user: this });
};

/**
 * Generate a new code and send it to the user's email.
 * Throws Mailer/Mongoose errors.
 */
schema.methods.sendVerificationCode = async function () {
  this.verification.code = this.constructor.generateVerificationCode();
  await this.save();

  return await Mailer.sendFromTemplate('validate', {
    from: process.env.MAIL_USER,
    to: this.email,
    subject: 'Account validation',
  }, { code: this.verification.code.tok, user: this });
};

schema.methods.getPublicData = function () {
  return ({
    _id: this._id,
    username: this.username,
    picture: this.picture,
    name: { ...this.name },
  });
};

schema.methods.getPersonalData = function () {
  let data = this.toObject();

  delete data.password;
  delete data.verification;

  return data;
};

module.exports = mongoose.model('user', schema);
