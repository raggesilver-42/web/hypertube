const nodemailer  = require('nodemailer');
const pug         = require('pug');
const path        = require('path');

const config = {
  app_url: process.env.APP_URL,
  app_name: process.env.APP_NAME,
};

function getTransporter() {
  return nodemailer.createTransport({
    service: 'gmail',
    auth: {
      user: process.env.MAIL_USER,
      pass: process.env.MAIL_PASS,
    },
  });
}

module.exports = {
  /**
   *
   * @param {String} templateName name of the template, throws error if not exists
   * @param {Object} mailOptions object containing 'from', 'to' and 'subject'
   * @param {Object} locals object containing vairables for email template
   */
  async sendFromTemplate(templateName, mailOptions, locals = {}) {
    let contentFunction = pug.compileFile(
      path.join(__dirname, 'templates', templateName + '.pug'));

    // Add project config to mail locals
    locals = { ...locals, ...config };
    let content = contentFunction(locals);

    mailOptions.html = content;
    let transporter = getTransporter();

    return await transporter.sendMail(mailOptions);
  },
};
