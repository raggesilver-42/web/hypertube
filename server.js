const express      = require('express');
const mongoose     = require('mongoose');
const bodyParser   = require('body-parser');
const history      = require('connect-history-api-fallback');
const path         = require('path');
const cors         = require('cors');
const app          = express();

/**
 * Initialize the app
 */
app.use(cors({ credentials: true, origin: true }));
const historyMiddleware = history();
// Do redirect /api calls to front-end
app.use((req, res, next) => {
  if (req.path.startsWith('/api'))
    next();
  else
    historyMiddleware(req, res, next);
});

app.use(bodyParser.json({ limit: '50mb' }));
app.use(bodyParser.urlencoded({ extended: false }));

app.use(express.static(path.join(__dirname, 'front', 'dist')));

const mongoOpts = {
  useNewUrlParser: true,
  useUnifiedTopology: true,
  // reconnectInterval: 30000, // Try to reconnect after 30s
  // reconnectTries: 600, // Just be a little more persistent (try for 5 hours)
};

/**
 * Start the app after mongoose connected
 */
mongoose.connect(process.env.DB_URL, mongoOpts)
  .then(() => {

    app.use('/api', require('./app/router'));

    app.listen(process.env.PORT || 3000, function () {
      console.log(`Server running on port ::${this.address().port}`);
    });
  })
  .catch(err => console.log(err));
